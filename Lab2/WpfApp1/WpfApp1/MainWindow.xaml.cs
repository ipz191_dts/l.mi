﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Timers;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : System.Windows.Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        #region Variables
        Random x = new Random();
        Random y = new Random();
        public System.Windows.Point p = new System.Windows.Point();
        public System.Windows.Point[] pi = new System.Windows.Point[2];
        int Status_exp = 0;
        DateTime Start;
        DateTime Stoped;
        TimeSpan Elapsed = new TimeSpan();
        public System.Windows.Point s;
        Excel.Application ex = new Excel.Application();
        #endregion

        private void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            lbRez.Items.Clear();
            lbRez.Items.Add("Результат");
            expir();
        }

        private System.Windows.Point DrowObject(System.Windows.Point p)
        {
            int D = 9;
            System.Windows.Shapes.Rectangle el = new System.Windows.Shapes.Rectangle();
            el.Width = D * 3;
            el.Height = D;
            p.X = x.Next(Convert.ToInt32(this.Width - el.Width));
            p.Y = y.Next(Convert.ToInt32(icDrow.ActualHeight - el.Height));
            el.Fill = Brushes.Blue;
            InkCanvas.SetLeft(el, p.X);
            InkCanvas.SetTop(el, p.Y);
            icDrow.Children.Add(el);
            return p;
        }

        public void expir()
        {
            int[] rez_arr = new int[2];
            Array.Clear(pi, 0, pi.Length - 1);
            Start = new DateTime(0);
            icDrow.Children.Clear();
            icDrow.Strokes.Clear();
            for(int i = 0; i < 2; i++)
            {
                pi[i] = DrowObject(p);
            }
            Status_exp = Status_exp + 1;
        }

        private void icUp(object sender, MouseButtonEventArgs e)
        {
            if (Status_exp > 0)
            {
                System.Windows.Point[] n = Array.FindAll(pi, element => (Math.Abs(element.X - e.GetPosition(this).X) < 20) && (Math.Abs(element.Y - e.GetPosition(this).Y) < 20));
                if (Array.Exists(pi, element => (Math.Abs(element.X - e.GetPosition(this).X) < 20) && (Math.Abs(element.Y - e.GetPosition(this).Y) < 20)))
                {
                    if (!((Math.Abs(s.X - e.GetPosition(this).X) < 10) && (Math.Abs(s.Y - e.GetPosition(this).Y) < 10)))
                    {
                        s = e.GetPosition(this);
                        {
                            if (Start.Ticks == 0)
                            {
                                Start = DateTime.Now;
                            }
                            else
                            {
                                Stoped = DateTime.Now;
                                Elapsed = Stoped.Subtract(Start);
                                long elapsedTicks = Stoped.Ticks - Start.Ticks;
                                TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);
                                double rez = Math.Sqrt(Math.Pow(pi[0].X - pi[1].X, 2) + Math.Pow(pi[0].Y - pi[1].Y, 2));
                                lbRez.Items.Add("Eксперемент" + Status_exp.ToString() + "Час: " + elapsedSpan.Milliseconds.ToString() + " Відстань " + rez);
                                
                                Excel.Workbook workbook = ex.Workbooks.Add(Type.Missing);
                                Excel.Worksheet sheet = (Excel.Worksheet)ex.Worksheets.get_Item(1);
                                sheet.Name = "Lab2";
                                sheet.Cells[Status_exp, 1] = String.Format("{0}", Status_exp.ToString());
                                sheet.Cells[Status_exp, 2] = String.Format("{0}", elapsedSpan.Milliseconds.ToString());
                                sheet.Cells[Status_exp, 3] = String.Format("{0}", Math.Round(rez));
                                ex.Application.ActiveWorkbook.SaveAs("C:\\Users\\Incore\\Desktop\\Lab2.xlsx");
                                if (Status_exp < 100)
                                    expir();
                            }
                        }
                    }
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
        }
    }
}

