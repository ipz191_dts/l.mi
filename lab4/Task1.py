import requests
from bs4 import BeautifulSoup
import time
import smtplib

class Currency:
    dollar_grn = 'https://www.google.com/search?q=%D0%B4%D0%BE%D0%BB%D0%B0%D1%80+%D0%B4%D0%BE+%D0%B3%D1%80%D0%B8%D0%B2%D0%BD%D1%96&rlz=1C1CHZL_ruUA760UA760&oq=%D0%B4%D0%BE%D0%BB%D0%B0%D1%80+%D0%B4%D0%BE+%D0%B3%D1%80%D0%B8%D0%B2%D0%BD%D1%96&aqs=chrome..69i57j0i512l2j0i457i512j0i512l6.9024j1j7&sourceid=chrome&ie=UTF-8'
    headers = {'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36'}
    current_converted_price = 0
    difference = 5

    def __init__(self):
        self.current_converted_price = float(self.get_currency_price().replace(',', '.'))
    def get_currency_price(self):
        full_page = requests.get(self.dollar_grn, headers=self.headers)
        soup = BeautifulSoup(full_page.content, 'html.parser')
        convert = soup.findAll("span", {"class": "DFlfde", "class": "SwHCTb", "data-precision":2})
        return convert[0].text
    def check_currency(self):
        currency = float(self.get_currency_price().replace(',', '.'))
        if currency >= self.get_currency_price + self.difference:
            print('Курс сильно виріс, можливо пора щось робити?')
        elif currency <= self.current_converted_price - self.difference:
            print('Курс сильно впав, можливо пора щось робити?')
            self.send_mail()

        print("Зараз курс 1$ = " + str(currency))
        time.sleep(3)
        self.check_currency()
    def send_mail(self):
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login('Post', 'Pass')

        subject = 'Currency mail'
        body = 'Currency has been changed!'
        message = f'Subject: {subject}\n{body}'

        server.sendmail('To:', 'From:', message)
        server.quit()

currency = Currency()
currency.check_currency()