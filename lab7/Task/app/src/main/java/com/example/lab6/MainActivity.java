package com.example.lab6;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView tvColor, tvSize, tv;
    CheckBox chb;

    final int MENU_COLOR_RED = 1;
    final int MENU_COLOR_GREEN = 2;
    final int MENU_COLOR_BLUE = 3;
    final int MENU_COLOR_A = 4;
    final int MENU_SIZE_22 = 5;
    final int MENU_SIZE_26 = 6;
    final int MENU_SIZE_30 = 7;
    final int MENU_SIZE_34 = 8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv = (TextView) findViewById(R.id.textView);
        chb = (CheckBox) findViewById(R.id.chbExtMenu);


        tvColor = (TextView) findViewById(R.id.tvColor);
        tvSize = (TextView) findViewById(R.id.tvSize);
        registerForContextMenu(tvColor);
        registerForContextMenu(tvSize);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, "add");
        menu.add(0, 2, 0, "edit");
        menu.add(0, 3, 3, "delete");
        menu.add(1, 4, 1, "copy");
        menu.add(1, 5, 2, "paste");
        menu.add(1, 6, 4, "exit");
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.setGroupVisible(1, chb.isChecked());
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        String sb = "Item Menu" +
                "\r\n groupId: " + String.valueOf(item.getGroupId()) +
                "\r\n itemId: " + String.valueOf(item.getItemId()) +
                "\r\n order: " + String.valueOf(item.getOrder()) +
                "\r\n title: " + item.getTitle();
        tv.setText(sb);
        return super.onOptionsItemSelected(item);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        switch (v.getId()) {
            case R.id.tvColor:
                menu.add(0, MENU_COLOR_RED, 0, "C/C++");
                menu.add(0, MENU_COLOR_GREEN, 0, "Python");
                menu.add(0, MENU_COLOR_BLUE, 0, "PHP");
                menu.add(0, MENU_COLOR_A, 0, "C#");
                break;
            case R.id.tvSize:
                menu.add(0, MENU_SIZE_22, 0, "1");
                menu.add(0, MENU_SIZE_26, 0, "2");
                menu.add(0, MENU_SIZE_30, 0, "3");
                menu.add(0, MENU_SIZE_34, 0, "4 и больше...");
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_COLOR_RED:
                tvColor.setText("C/C++");
                break;
            case MENU_COLOR_GREEN:
                tvColor.setText("Python");
                break;
            case MENU_COLOR_BLUE:
                tvColor.setText("PHP");
                break;
            case MENU_COLOR_A:
                tvColor.setText("C#");
                break;
            case MENU_SIZE_22:
                tvSize.setText("1");
                break;
            case MENU_SIZE_26:
                tvSize.setText("2");
                break;
            case MENU_SIZE_30:
                tvSize.setText("3");
                break;
            case MENU_SIZE_34:
                tvSize.setText("4 и больше...");
                break;
        }
        return super.onContextItemSelected(item);
    }
}