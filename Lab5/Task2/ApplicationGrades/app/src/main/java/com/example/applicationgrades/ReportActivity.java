package com.example.applicationgrades;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ReportActivity extends AppCompatActivity {
    public static final String STUDENTS_DATA = "Hello";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        Intent intent = getIntent();
        String MessageText = intent.getStringExtra(STUDENTS_DATA);
        TextView tv = (TextView) findViewById(R.id.textStudent);

        Bundle extras = intent.getExtras();
        if(extras!=null){
            String output =
                    "Name: "+extras.getString("Name")+'\n'+
                    "Group: "+extras.getString("Group")+'\n'+
                    "Subject: "+extras.getString("Subject")+'\n'+
                    "Test\\Exam: "+extras.getString("Test_Exam")+'\n'+
                    "Mark: "+extras.getString("Mark");

            tv.setText(output);
        }

    }
}
