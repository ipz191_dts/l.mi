package com.example.applicationgrades;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AutoCompleteTextView textView = (AutoCompleteTextView) findViewById(R.id.acTWSubject);
        String[] subjects = getResources().getStringArray(R.array.name_Subject);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, subjects);
        textView.setAdapter(adapter);
    }

    public void SaveRecord(View view){
        Intent intent = new Intent(this, ReportActivity.class);

        intent.putExtra("Name", ((EditText)findViewById(R.id.Stud_name)).getText().toString());
        intent.putExtra("Group", ((Spinner)findViewById(R.id.spGroup)).getSelectedItem().toString());
        intent.putExtra("Subject", ((AutoCompleteTextView)findViewById(R.id.acTWSubject)).getText().toString());
        intent.putExtra("Test_Exam", ((RadioButton)findViewById(R.id.rbZalik)).isChecked()?"Test":"Exam");
        intent.putExtra("Mark", ((EditText)findViewById(R.id.etReiting)).getText().toString());

        startActivity(intent);
    }
}
