package com.example.myapplicationl;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.*;
import android.view.View;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    TextView tvInfo;
    EditText etInput;
    Button bControl;

    boolean gameStarted = false,
            firstLaunch = true;
    int     currentCycle = 0,
            gameCycle = 0,
            guessValue = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvInfo = (TextView) findViewById(R.id.textView2);
        etInput = (EditText) findViewById(R.id.editText);
        bControl = (Button) findViewById(R.id.button);
    }

    public void onClick(View v) {
        if(!gameStarted){
            tvInfo.setText(getResources().getString(R.string.try_to_guess));
            bControl.setText(getResources().getString(R.string.input_value));

            gameStarted = true;
            gameCycle++;
        }

        if(gameStarted && currentCycle!=gameCycle){
            currentCycle = gameCycle;
            guessValue = (new Random()).nextInt(11);

            if(!firstLaunch) return;
        }


        if(!etInput.getText().toString().matches("")) {
            int input = Integer.parseInt(etInput.getText().toString());

            if(input<guessValue){
                tvInfo.setText(getResources().getString(R.string.behind));
            } else if(input>guessValue) {
                tvInfo.setText(getResources().getString(R.string.ahead));
            } else {
                tvInfo.setText(getResources().getString(R.string.hit));
                bControl.setText(getResources().getString(R.string.play_more));

                gameStarted = false;
            }
        } else {
            tvInfo.setText(getResources().getString(R.string.error));
        }

        if(firstLaunch)firstLaunch=false;
    }
}
